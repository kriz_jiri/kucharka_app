/**
 * Created by jirikriz on 10.12.16.
 */

//zobrazí informace o uživateli
function getAccountInfo() {

}

$(document).on('submit','#posliRecept',function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var url = "vlozRecept.php"; // the script where you handle the form input.
    var formData = new FormData($(this)[0]);
    $('#novyRecept').hide();
    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data: formData, // serializes the form's elements.
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,
        success: function(odpoved) {
            $("#pozadiReceptu").hide();
            nactiMojeRecepty();
            zavriRecept();
            vypisZpravu(odpoved);
        }
    });
});

function nactiMojeRecepty() {
    $("#loader").show();
    $.ajax({
        url:'nactiMojeRecepty.php',
        type:'GET',
        data: "",
        success:function(data){
            $("#container").empty();
            $("#container").append(data).children().hide();
            $('#container').imagesLoaded().done( function() {
                // images have loaded
                $("#loader").hide();
                $("#container").children().show();
            });
        }
    });
}


function nactiVsechnyRecepty() {
    $("#loader").show();
    $.ajax({
        url:'receptyFiltr.php',
        type:'GET',
        data: "",
        success:function(data){
            $("#container").empty();
            $("#container").append(data).children().hide();
            $('#container').imagesLoaded().done( function() {
                // images have loaded
                $("#loader").hide();
                $("#container").children().show();
            });
        }
    });
}

$('#pages li').click(function () {
    $("#loader").show();
    var pageNumber = $(this).attr("id");
    $(this).siblings().css("background-color", "#e67e22");
    $(this).css("background-color", "#c0392b");
    $.ajax({
        url:'inc/paginator.php',
        type:'GET',
        data: {page: pageNumber},
        success:function(data){
            $("#loader").hide();
            $("#container").empty();
            $("#container").append(data);
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    });
});

function nactiOblibeneRecepty() {
    $.ajax({
        url: 'nactiOblibene.php',
        type: 'GET',
        data: "",
        success: function (data) {
            $("#container").empty();
            $("#container").append(data);
            $("#filtrReceptu, #zobrazFiltr").hide();
        }
    });
}

function nactiRecept() {
    $.ajax({
        url:'nactiRecept.php',
        type:'GET',
        data: "",
        success:function(data){
            $("#content").empty().append(data);
        }
    });
}

$('#filtrReceptu').submit(function(e){
    e.preventDefault();
    $('#pages').hide();
    $.ajax({
        url:'receptyFiltr.php',
        type:'GET',
        data:$('#filtrReceptu').serialize(),
        success:function(data){
            $("#container").empty();
            $("#container").append(data);
        }
    });
});

//po zrušení filteru načte všechny recepty
$('body').on( "click","#zrusFiltr", function() {
    nactiVsechnyRecepty();
    $('#pages').show();
});

//pošle AJAX request pro smazání receptu
function smazRecept(receptID) {
    $.ajax({
        url: 'smazRecept.php',
        type: 'POST',
        data: {delete: receptID},
        success: function (odpoved) {
            nactiMojeRecepty();
            vypisZpravu(odpoved);
        }
    });
}

$("#thumb_up").click(function () {
    $.ajax({
        url:'hodnoceniReceptu.php',
        type:'POST',
        data: {thumb_up: ""},
        success:function(odpoved){
            aktualizujHodnoceni(odpoved);
        }
    });
});

$("#thumb_down").click(function () {
    $.ajax({
        url:'hodnoceniReceptu.php',
        type:'POST',
        data: {thumb_down: ""},
        success:function(odpoved){
            aktualizujHodnoceni(odpoved);
        }
    });
});

$("#bookmark").click(function () {
    $.ajax({
        url:'pridatDoOblibenych.php',
        type:'POST',
        success:function (odpoved) {
            vypisZpravu(odpoved);
        }
    });
});

function vlozDoOblibenych(recept_id) {
    $.ajax({
        url:'pridatDoOblibenych.php',
        type:'POST',
        data: {recept_id: recept_id},
        success:function (odpoved) {
            vypisZpravu(odpoved);
        }
    });
}

$("body").on("click","#edit-button", function () {
    $.ajax({
        //Pošli request na informace o receptu - podle id receptu
        url:'vratRecept.php',
        type:'POST',
        data: {recept: ""},
        success:function(odpoved){
            var odpoved = odpoved.split("!!");
            var recept = odpoved[0].split("?");
            var ingredience = odpoved[1].split("?");
            naplnFormular(recept, ingredience[0]);
            pridejRecept();
        }
    });
});
