/**
 * Created by jirikriz on 18.03.17.
 */
var cisloIngredience = 1;
var receptID = null;
$("#novyRecept, .alert, #loader").hide();
$('#chybaVlozeni, #uspesneVlozeni').hide();
$('#vratRecept').hide();

$("#pozadiReceptu").click(function () {
    zavriRecept();
});

$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        zavriRecept();
    }
});

function pridejRecept() {
  // $('#novyRecept #nzv, #novyRecept #pps').val("");
  $('#novyRecept').animateCss('fadeInDown');
  $("#novyRecept, #pozadiReceptu, #novyRecept form").show();
}

function zavriRecept() {
    $("#novyRecept").hide();
    var tableIngredience = `
                        <i class="material-icons close-btn" onclick="zavriRecept()">close</i>
                        <p id="postup" class="normal-mode"></p>
                        <form id="posliRecept">
                            <div class="input-group edit-mode col-md-7">
                                <label for="nzv">Název receptu:</label>
                                <input type="text" class="form-control" name="nazevReceptu" id="nzv" required>
                            </div>
                            <div class="input-group edit-mode col-md-8">
                                <label for="pps">Popis:</label>
                                <textarea class="form-control" name="popisReceptu" rows="7" id="pps" value="" required></textarea>
                            </div>
                            <div class="form-inline">
                                <div id="zeme-puvodu" class="input-group edit-mode col-md-4">
                                    <label for="zeme">Země původu:</label>
                                    <input type="text" class="form-control" name="zeme" id="zeme" required>
                                </div>
                                <div class="input-group edit-mode col-md-3">
                                    <label for="delka">Délka přípravy (minuty):</label>
                                    <input type="text" class="form-control" name="delka" id="delka" required>
                                </div>
                            </div>
                            <table id="ingredience">
                                <tr>
                                    <th>Ingredience</th>
                                    <th>Množství</th>
                                    <th>Jednotky</th>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" name="ingredience0" required></td>
                                    <td><input type="text" class="form-control" name="mnozstvi0" required></td>
                                    <td><select class="form-control" id="sel1" name="jednotky0">
                                        <option>ml</option>
                                        <option>g</option>
                                        <option>ks</option>
                                        <option>špetka</option>
                                        <option>lžíce</option>
                                        <option>stroužek</option>
                                    </select>
                                    </td>
                                </tr>
                                <tr id="ingredience-buttons">
                                    <td></td>
                                    <td><button id="pridejIngredienci" class="btn btn-default">Přidat</button></td>
                                    <td><button id="odeberIngredienci" class="btn btn-default">Odebrat</button></td>
                                </tr>
                            </table>
                            <div class="form-group">
                                <label for="img">Obrázek</label>
                                <input type="file" name="image" id="img" required>
                            </div>
                    <!--        <button type="submit" class="btn btn-default" id="done-btn">Uložit</button>-->
                            <button type="submit" class="btn btn-primary" id="done-btn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Recept se nahrává">Vložit recept</button>
                        </form>
                        `;


    $("#novyRecept").empty().append(tableIngredience);
    $("#pozadiReceptu").hide();
}

$("#signup").click(function (event) {
    event.preventDefault();
})

$("#novyRecept").on("click","#pridejIngredienci", function (event) {
    event.preventDefault();
    var novaIngredience =   '<tr>'
                                +'<td><input type="text" class="form-control" name="ingredience' +cisloIngredience +'" required></td>'
                                +'<td><input type="text" class="form-control" name="mnozstvi' +cisloIngredience +'" required></td>'
                                +'<td><select class="form-control" id="sel1" name="jednotky' +cisloIngredience +'">'
                                    +'<option>ml</option>'
                                    +'<option>g</option>'
                                    +'<option>ks</option>'
                                    +'<option>špetka</option>'
                                    +'<option>lžíce</option>'
                                    +'<option>stroužek</option>'
                                +'</select>'
                                +'</td>'
                            +'</tr>';
    $("#ingredience-buttons").before(novaIngredience);
    cisloIngredience++;
});

$("#novyRecept").on("click","#odeberIngredienci", function (event) {
    event.preventDefault();
    var pocetRadku = $('#ingredience tr:last').index();
    if(pocetRadku > 2) {
        $("#ingredience-buttons").prev().remove();
        cisloIngredience--;
    } else if (pocetRadku > 1) {
        var $radek = $("#ingredience-buttons").prev();
        $("#ingredience").find("input").val("");
    }
});

$("#container").on("click",".delete", function (event) {
    event.stopPropagation();
    receptID = $(this).parent().attr("id");
    $("#myModal").modal('show');
});


$('#myModal .btn-primary').click(function () {
    if (recept =! null) {
        smazRecept(receptID);
        $('#myModal').modal('hide');
    }
});

function naplnFormular(recept, ingredience){
    var options = ['ml', 'g', 'ks', 'špetka', 'lžíce', 'stroužek'];
    ingredience = JSON.parse(ingredience);

    for (var i = 0; i < ingredience.length; i++) {
        if (i == 0) {
            $("#ingredience tr:nth-child(2)").remove();
        }

        var vkladanaIngredience = "<tr>"
            + '<td> <input type="text" class="form-control" name="ingredience' + i + '" value ="' + ingredience[i][0] + '"  required></td>"'
            + '<td> <input type="text" class="form-control" name="mnozstvi' + i + '" value ="' + ingredience[i][1] + '"  required></td>"'
            + '<td> <select class="form-control" id="sel1" name="jednotky' + i + '" value ="' + ingredience[i][2] + '">'
        var jednotkaIngredience = ingredience[i][2];

        for (var j = 0; j < options.length; j++) {
            if (jednotkaIngredience === options[j]) {
                vkladanaIngredience += '<option selected>' + options[j] + '</option>';
            } else {
                vkladanaIngredience += '<option>' + options[j] + '</option>';
            }
        }

        vkladanaIngredience += '</select>'
                                + '</td>'
                                + "</tr>";
        console.log(vkladanaIngredience);

        $("#ingredience-buttons").before(vkladanaIngredience);
    }

    $('#posliRecept #nzv').val(recept[1]);
    $('#posliRecept #pps').val(recept[2]);
    $('#posliRecept #zeme').val(recept[3]);
    $('#posliRecept #delka').val(recept[4]);
    $("#posliRecept").show();
}

function aktualizujHodnoceni(odpoved) {
    $("#about li:nth-child(2)").empty().append('<i class="material-icons hodnoceni">local_dining</i>' +' ' +odpoved +"%");
};
