/**
 * Created by jirikriz on 10.12.16.
 */
var receptOtevren = false;
var oblibeneOtevreny = false;
var menuOtevreno = false;
var ucetOtevren = false;
var prvniVyber = 0;
var oblibeneRecepty = [];

$(".recept .delete, .alert").hide();
$("#pozadiReceptu").hide();
$("#pop-up").hide();
$('#pop-up-account').hide();
$('#chef-orange, #zadneOblibene, #filtrReceptu, #mobile-menu, #account_info').hide();
$('.clicked').show();



$("#zobrazFiltr").click(function(){
    $("#filtrReceptu").show();
    $(this).hide();
});

$("#filtrReceptu .close-btn").click(function(){
    $("#filtrReceptu").hide();
    $("#zobrazFiltr, .recept").show();
});

$("#brand").hover(function() {
    $(this).children("#chef-grey").hide();
    $(this).children("#chef-orange").show();
});

$("#brand").mouseleave(function() {
    $(this).children("#chef-orange").hide();
    $(this).children("#chef-grey").show();
});

//zobrazí fav po najetí myši
$("#container").on("mouseenter",".recept", function () {
    if (!$(".fav", this).hasClass("clicked")){
        $(".fav", this).show().fadeIn(200);
        // $('.fav', this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(".delete", this).show().fadeIn(200);
        // });
    }
});

//skryje fav po vyjetí myši
$("#container").on("mouseleave",".recept", function () {
    if (!$(".fav",this).hasClass("clicked")) {
        $(".fav", this).fadeOut(200);
        // $('.fav', this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(".delete", this).fadeOut(200);
        // });
    }
});

//pro zobrazení receptu
$("#container").on( "click",".recept", function() {
    var idName = $(this).attr('id');
    document.location = "recept.php?id="+idName;
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $("#pozadiReceptu").fadeIn(400);
    $("#pop-up").show();
    $("#pop-up").animateCss('zoomInUp');
    receptOtevren = "true";
});

$("#menu-icon a").click(function(event) {
    if (menuOtevreno) {
        $("#mobile-menu").animateCss("slideOutUp");
        $("#mobile-menu").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(evt){
            event.preventDefault();
            // evt.preventDefault();
            $(this).hide();
            menuOtevreno = false;
        });

    } else {
        $("#mobile-menu").show();
        $("#mobile-menu").animateCss("slideInDown");
        $("#mobile-menu").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(evt){
            event.preventDefault();
            // evt.preventDefault();
            menuOtevreno = true;
        });
    }
});

$("#mobile-menu *").click(function(){
    $(this).parent().hide();
    menuOtevreno = false;
});

//pro zobrazení informací o účtu uživatele
$("#login-icon").click(function () {
    if(ucetOtevren)  {
        // $("#account_info").animateCss('bounceOutUp');
        $("#account_info").hide();
        ucetOtevren = false;
        $(this).children().css('color', '#555');
    } else {
        getAccountInfo();
        $(this).children().css('color', '#c0392b');
        // $("#pozadiReceptu").show();
        $("#account_info").show();
        $("#account_info").animateCss('bounceInDown');
        ucetOtevren = true;
    }
});


//pro kliknutí na tlačítko oblíbené
$("#container").on("click",".fav", function (event) {
    event.stopPropagation(); //dojde pouze ke kliknutí na favorites, ne na celý element
    var idReceptu = $(this).parent().attr('id');
    if ($(this).hasClass("clicked")) {
        $(this).removeClass("clicked");
        $(this).css('background-color', '#e67e22');
        //zavolání AJAX
        vlozDoOblibenych(idReceptu);
    } else {
        //zavolání AJAX
        vlozDoOblibenych(idReceptu);
        $(this).animateCss('rubberBand');
        $(this).addClass("clicked");
        $(this).css('background-color', '#c0392b');
        $(this).show();

    }
    if(oblibeneOtevreny) {
        nactiOblibene();
    }
});

//pro výběr možností z filtru receptů
$(".btn-group .dropdown-menu li").click(function () {
        var $selectedNew = $("a",this).text();
        var $selectedOld = $(this).parent().siblings("button").text();
        $(this).parent().siblings("button").empty();
        $(this).parent().siblings("button").append($selectedNew);
        var temp = $selectedOld;
        if (prvniVyber === 0 && $(this).parentsUntil(".btn-group").attr("class").split(" ")[1] === "zemeFiltr") {
            $("a",this).empty();
            prvniVyber++;
        } else {
            $("a",this).empty().append(temp);
        }
});

// pro filtrování dle země původu
$(".zemeFiltr a").click(function(){
    var zeme = $(this).text();
    var idZeme;
    switch (zeme) {
        case "italské":
            idZeme = "it";
            break;
        case "francouzské":
            idZeme = "fr";
            break;
        case "mexické":
            idZeme = "mx";
            break;
        default:
    }
    $(".recept").hide();
    $(".recept").each(function(){
        var $this = $(this);
        var $id = $this.attr('class').split(' ')[1];
        if($id === idZeme) {
          $this.show();
        }
    });
});

$('#bookmark').click(function () {
    if ($(this).hasClass("clicked")) {
        $(this).css("background-color", "#e67e22").removeClass("clicked");
    } else {
        $(this).css("background-color", "#c0392b").addClass("clicked");
    }
});

$('#bookmark, #thumb_up, #thumb_down').click(function () {
   $(this).animateCss("rubberBand");
});


// Funkce pro vypisování upozornění ze stavu v backendu
function vypisZpravu(data) {
    var vypis = "";
    switch (data) {
        case "uspech":
            vypis = $("#uspech").text("Recept byl úspěšně vložen");
            break;
        case "aktualizace":
            nactiRecept();
            vypis = $("#uspech").text("Recept byl úspěšně upraven");
            break;
        case "chyba-vlozeni":
            vypis = $("#chyba").text("<strong>Chyba!</strong> Vyplňte všechna pole formuláře");
            break;
        case "smazan-uspech":
            vypis = $("#uspech").text("Recept byl úspěšně smazán");
            break;
        case "smazan-neuspech":
            vypis = $("#chyba").text("Recept nemohl být smazán");
            break;
        case "uspech-oblibene-pridan":
            vypis = $("#uspech").text("Recept byl přidán do oblíbených");
            break;
        case "neuspech-oblibene-nepridan":
            vypis = $("#chyba").text("Recept se nepodařilo přidat");
            break;
        case "uspech-oblibene-odebran":
            vypis = $("#uspech").text("Recept byl odebrán z oblíbených");
            break;
        case "chyba-neni-prihlasen":
            vypis = $("#chyba").text("Není přihlášen žádný uživatel, recept nelze přidat do oblíbených");
            break;
        case "neni_obrazek":
            vypis = $("#chyba").text("Vkládaný soubor musí být obrázek!");
            break;
        default:
            vypis = $("#chyba").text(data);
    }
    vypis.stop( true, true ).fadeIn(700).delay(1500).fadeOut(1500);
}

//rozšíření jQuery o funkci animateCss
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

$(window).resize(function(){
       if ($(window).width() >= 600) {
           $("#mobile-menu").hide();
           menuOtevreno = false;
       }
});
