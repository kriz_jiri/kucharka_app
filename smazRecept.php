<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 09.04.17
 * Time: 20:00
 */

session_start();
require 'inc/Database.php';
$database = new Database();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["delete"])) {
        $receptID = trim(filter_input(INPUT_POST, "delete", FILTER_SANITIZE_NUMBER_INT));
        $uzivatel_id = $database->getUzivatelByReceptID($receptID);
        if ($uzivatel_id == $_SESSION["user_id"]) {
            $receptSmazan = $database->smazRecept($receptID);
            if($receptSmazan) {
                echo "smazan-uspech";
            } else {
                echo "smazan-neuspech";
            }
        }
    }
}