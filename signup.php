<?php
session_start();
require 'inc/Uzivatele.php';
$uzivatel = new Uzivatele();


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["email"])
        && isset($_POST["firstname"])
        && isset($_POST["surname"])
        && isset($_POST["password"])) {

        $email = trim(filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING, FILTER_VALIDATE_EMAIL));
        $firstname = trim(filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_STRING));
        $surname = trim(filter_input(INPUT_POST, "surname", FILTER_SANITIZE_STRING));
        $password = trim(filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING));
        $uzivatel->registrujUzivatele($email, $firstname, $surname, $password);
    } else {
        header("Location: /index.php");
    }
} else {
    if (isset($_GET["logout"])) {
        $uzivatel->odhlasUzivatele();
    }
}