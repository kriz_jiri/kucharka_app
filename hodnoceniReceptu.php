<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 10.04.17
 * Time: 20:48
 */
session_start();
require 'inc/Database.php';
$database = new Database();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $recept_id = $_SESSION["recept_id"];
    if (isset($_POST["thumb_up"])) {
        $database->thumb_up($recept_id);
    } elseif (isset($_POST["thumb_down"])) {
        $database->thumb_down($recept_id);
    }
    $thumbs = $database->getThumbsByID($recept_id);
    $response =  $database->setHodnoceni(intval($thumbs["thumbs_up"]),
                            intval($thumbs["thumbs_down"]),
                            $recept_id);
}