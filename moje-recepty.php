<?php
session_start();

$_SESSION["activePage"] = "mojeRecepty";

if(isset($_SESSION["recept_id"])) {
    unset($_SESSION["recept_id"]);
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta charset="UTF-8">
    <title>Kuchařka</title>
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicon/favicon.ico">
    <meta name="msapplication-config" content="favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style-moje-recepty.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/style-sign-form.css">
    <link rel="stylesheet" href="css/style-recept-form.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</head>
<body>
    <div class="alert alert-danger" id="chyba"></div>
    <div class="alert alert-success" id="uspech"></div>
    <?php   require 'inc/header.php';
            require 'inc/sign-form.php'?>
    <div id="loader"></div>
    <div id="pridej-recept">
        <button class="btn btn-default btn-lg" onclick="pridejRecept()"><i class="glyphicon glyphicon-plus"></i></button>
        <button id="vratRecept" class="btn btn-default btn-lg" onclick="vratRecept()"><i class="glyphicon glyphicon-arrow-left"></i></button>
    </div>
    <div id="container">
    </div>
    <?php require 'inc/recept-form.php';?>
    <div id="pozadiReceptu" onclick="zavriRecept()"/>

    <?php require 'inc/modal-potvrzeni.php';?>
    <?php require "inc/footer.php";?>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/moje-recepty.js"></script>
    <script src="js/signup.js"></script>
    <script src="js/ajax.js"></script>
    <script type="text/javascript">
        nactiMojeRecepty();
    </script>
</body>
</html>
