<?php
session_start();

if(isset($_SESSION["recept_id"])) {
    unset($_SESSION["recept_id"]);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta charset="UTF-8">
    <title>Kuchařka</title>
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicon/favicon.ico">
    <meta name="msapplication-config" content="favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style-recept.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/style-sign-form.css">
    <link rel="stylesheet" href="css/pages.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</head>
<body>
    <?php require 'inc/header.php';
        require 'inc/sign-form.php'
    ?>
    <div id="loader"></div>

    <div class="alert alert-danger" id="chyba"></div>
    <div class="alert alert-success" id="uspech"></div>
    <div id="zadneOblibene" class="alert alert-info">
      Do oblíbených nebyly zatím přidány žádné recepty.
    </div>
    <form id="filtrReceptu" class="form-inline">
            <select name="order">
                <option value="hodnoceni">Nejlépe hodnocené</option>
                <option value="cas">Nejnovější</option>
                <option value="rychlost">Nejrychlejší</option>
            </select>
        <span>recepty</span>
        <select name="zeme">
            <option value="it">Italské</option>
            <option value="fr">Francouzské</option>
            <option value="mx">Mexické</option>
        </select>
    <span>kuchyně</span>
    <button type="submit" class="btn btn-default">Submit</button>
    <i id="zrusFiltr" class="material-icons close-btn">close</i>
    </form>
    <div id="zobrazFiltr">Filtr receptů</div>
    <div id="container">
    </div>
    <div id="pozadiReceptu" onclick="zavriRecept()">
    </div>
    </div> <!--Pro zobrazení receptů-->
    <div id="pop-up-account"></div> <!--Pro zobrazení informací o účtu-->
    <?php require "inc/paginator.php";?>
    <?php require "inc/footer.php";?>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/app.js"></script>
    <script src="js/signup.js"></script>
    <script>
        $('.delete').hide();
        nactiVsechnyRecepty();
    </script>
</body>
</html>