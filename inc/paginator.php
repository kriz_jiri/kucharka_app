<?php
require "Database.php";
$database = new Database();
$page = 1;
$limit = 9;

if (isset($_GET["page"])) {
    $page = trim(filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT));
    $offset = ($page - 1)  * $limit;
    $database->getRecepty($offset);

} else {
    $pocetReceptu = $database->getPocetReceptu();
    $pages = ceil($pocetReceptu / $limit);

    echo '<ul id="pages">';
    for ($i = 1; $i <= $pages; $i++) {
        if ($i == 1) {
            echo "<li id='$i' style='background-color: #c0392b'>$i</li>";
        } else {
            echo "<li id='$i'>$i</li>";
        }
    }
    echo '</ul>';
}