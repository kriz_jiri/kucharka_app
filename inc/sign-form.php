<div id="account_info">
        <?php if(isset($_SESSION["user"])) {?>
    <p id=uzivatel>Přihlášen jako: <?php echo $_SESSION["user"]?></p>
    <form action="signin.php">
        <input type="hidden" name="logout" value="true"/>
        <button type="submit" class="btn btn-default" id="done-btn">Odhlásit</button>
    </form>
    </form>
    <?php
} else {
    ?>
    <form id="signin-form" action="signin.php" method="post">
        <div class="input-group edit-mode">
            <label for="nzv">Email:</label>
            <input type="text" class="form-control" name="username" id="username" required>
        </div>
        <div class="input-group edit-mode">
            <label for="nzv">Heslo:</label>
            <input type="password" class="form-control" name="password" id="password" required>
        </div>
        <button type="submit" class="btn btn-default">Přihlásit</button>
        <button id="signup-button" class="btn btn-default">Registrovat</button>
    </form>
<?php }?>
</div>

<div id="signup">
    <form id="signup-form" action="signup.php" method="post">
        <div class="input-group edit-mode">
            <label for="nzv">Email:</label>
            <input type="text" class="form-control" name="email" id="email" required>
        </div>
        <div class="input-group edit-mode">
            <label for="nzv">Jméno:</label>
            <input type="text" class="form-control" name="firstname" id="firstname" required>
        </div>
        <div class="input-group edit-mode">
            <label for="nzv">Příjmení:</label>
            <input type="text" class="form-control" name="surname" id="surname" required>
        </div>
        <div class="input-group edit-mode">
            <label for="nzv">Heslo:</label>
            <input type="password" class="form-control" name="password" id="password2" required>
        </div>
        <button id="signup-confirm" class="btn btn-default">Registrovat</button>
</form>
</div>