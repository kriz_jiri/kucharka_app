<?php

    require 'data.php';
    /** @var TYPE_NAME $vsechyRecepty */
    $vsechyRecepty= "";
    foreach ($response as $recept) {
        $id = $recept["recept_id"];
        $nazev = $recept["nazev"];
        $znacka =  $recept["zeme_puvodu"];
        $delka = $recept["delka_pripravy"];
        $obtiznost = $recept["obtiznost"];

        $vsechyRecepty .= "<div id=\"recept$id\" class=\"recept $znacka\" style=\"background-image: url('img/$id.jpg')\">"
                        ."<i class=\"material-icons fav\">bookmark</i>"
                            ."<div class=\"recept_info\">"
                                ."<div class=\"popis_receptu\">$nazev</div>"
                                ."<div><i class=\"material-icons\">timer</i>: $delka m</div>"
                                ."<div>Obtížnost:";

        for ($i = 0; $i < $obtiznost; $i++) {
            $vsechyRecepty.= "<i class=\"material-icons\">star</i>";
        }

        $vsechyRecepty .=  "</div> </div> </div>"
                            ." ";
    }
    echo $vsechyRecepty;
?>