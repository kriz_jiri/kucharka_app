<div id="novyRecept">
    <i class="material-icons close-btn" onclick="zavriRecept()">close</i>
    <p id="postup" class="normal-mode"></p>
    <form id="posliRecept">
        <div class="input-group edit-mode col-md-7">
            <label for="nzv">Název receptu:</label>
            <input type="text" class="form-control" name="nazevReceptu" id="nzv" required>
        </div>
        <div class="input-group edit-mode col-md-8">
            <label for="pps">Popis:</label>
            <textarea class="form-control" name="popisReceptu" rows="7" id="pps" required></textarea>
        </div>
        <div class="form-inline">
            <div id="zeme-puvodu" class="input-group edit-mode col-md-4">
                <label for="zeme">Země původu:</label>
                <input type="text" class="form-control" name="zeme" id="zeme" required>
            </div>
            <div class="input-group edit-mode col-md-3">
                <label for="delka">Délka přípravy (minuty):</label>
                <input type="text" class="form-control" name="delka" id="delka" required>
            </div>
        </div>
        <table id="ingredience">
            <tr>
                <th>Ingredience</th>
                <th>Množství</th>
                <th>Jednotky</th>
            </tr>
            <tr>
                <td><input type="text" class="form-control" name="ingredience0" required></td>
                <td><input type="text" class="form-control" name="mnozstvi0" required></td>
                <td><select class="form-control" id="sel1" name="jednotky0">
                    <option>ml</option>
                    <option>g</option>
                    <option>ks</option>
                    <option>špetka</option>
                    <option>lžíce</option>
                    <option>stroužek</option>
                </select>
                </td>
            </tr>
            <tr id="ingredience-buttons">
                <td></td>
                <td><button id="pridejIngredienci" class="btn btn-default">Přidat</button></td>
                <td><button id="odeberIngredienci" class="btn btn-default">Odebrat</button></td>
            </tr>
        </table>
        <div class="form-group">
            <label for="img">Obrázek</label>
            <input type="file" name="image" id="img" required>
        </div>
<!--        <button type="submit" class="btn btn-default" id="done-btn">Uložit</button>-->
        <button type="submit" class="btn btn-primary" id="done-btn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Recept se nahrává">Vložit recept</button>
    </form>
</div>