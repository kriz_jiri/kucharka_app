
<!-- Modal Potvrzení -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Potvrzení</h4>
            </div>
            <div class="modal-body">
                <p>Opravdu chce smazat recept?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
                <button type="button" class="btn btn-primary">Ano</button>
            </div>
        </div>
    </div>
</div>