<?php
    $pageName = basename($_SERVER["SCRIPT_FILENAME"], '.php');
    if ($pageName == "index") {
        $name = "Kuchařka";
        $currentPage = "index.php";
        $pageLink = "moje-recepty.php";
        $pageLinkName = "Moje Recepty";
    } else if ($pageName == "moje-recepty") {
        $name = "Moje Recepty";
        $currentPage = "moje-recepty.php";
        $pageLink = "index.php";
        $pageLinkName = "Kuchařka";
    } else if($pageName =="recept") {
        $name = "Kuchařka";
        $currentPage = "index.php";
        $pageLink = "moje-recepty.php";
        $pageLinkName = "Moje Recepty";
    }
?>

<header>
    <a id="brand" href="<?php echo $currentPage?>">
        <img id="chef-grey" class="chef-logo" src="./svg/chef.svg" alt="Chef logo"/>
        <img id="chef-orange" class="chef-logo" src="./svg/chef-orange.svg" alt="Chef logo"/>
        <h1><?echo $name?></h1>
    </a>
    <i class="material-icons" id="login-icon" onclick="zobrazUcet()"><a href="#">account_circle</a></i>
    <i class="material-icons" id="fav-icon" onclick="nactiOblibeneRecepty()"><a href="#">bookmark_border</a></i>
    <i class="material-icons" id="menu-icon"><a href="#"></a></i>
    <ul class="main-nav">
        <?php
        if (isset($_SESSION["user"])) {
            echo "<li><a href=\"$pageLink\">$pageLinkName</a></li>";
        }?>
        <!-- <li><a id="oblibene"  onclick="nactiOblibene()" href="#">Oblíbené</a></li> -->
    </ul>
</header>
<ul id="mobile-menu">
    <li onclick="zobrazUcet()">Účet</li>
    <li onclick="nactiOblibene()">Oblíbené</li>
    <li><a href="moje-recepty.php">Moje recepty</a></li>
</ul>