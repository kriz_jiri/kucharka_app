<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 08.04.17
 * Time: 22:47
 */

require 'Database.php';


class Uzivatele
{
    private $database;

    function __construct()
    {
        $this->database = new Database();
    }

    function prihlasUzivatele($username, $password) {
        if ($username != "" && $password != "") {
            $result = $this->database->getUzivatelID($username);
            $user_id = $result[0];
            $hashed = $result[1];
            if($user_id == null) {
                echo "Uživatel $username neexistuje";
                die();
            } else if(password_verify($password, $hashed)) {
                $_SESSION["user"] = $username;
                $_SESSION["user_id"] = $user_id;
            } else {
                echo "Bylo vloženo nesprávné heslo";
            }
//            setcookie("user", $username, time()+3600, "/");
            header("Location: ./index.php");
        } else {
            header("Location: /index.php");
        }
    }

    function odhlasUzivatele() {
        unset($_SESSION['user']);
        unset($_SESSION['user_id']);
        header("Location: ./index.php");
    }

    function registrujUzivatele($email, $firstname, $surname, $password) {
        if ($email != "" && $firstname != "" && $surname != "" && $password != "") {
            $hashed = password_hash($password, PASSWORD_DEFAULT);
            $uzivatelVlozen = $this->database->vlozUzivatele($email, $firstname, $surname, $hashed);
            if($uzivatelVlozen) {
                header("Location: /index.php");
            } else {
                echo "Vkládaný uživatel již existuje";
            }
        }
    }
}