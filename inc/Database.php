<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 24.03.17
 * Time: 18:57
 */

include 'ChromePhp.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class Database
{
    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=kucharka;charset=utf8', 'root', 'root');
        //$this->db = new PDO('mysql:host=127.0.0.1;dbname=krij01;charset=utf8', 'krij01', 'ayMElRXbhZhTw1je0a');
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    function getRecepty($offset) {
        //$offset = intval($offset);
        $response = $this->db->query("SELECT * FROM recept LIMIT 9 OFFSET $offset");
        $response->execute(array($offset));
        $result = $response->fetchAll();
        $this->vypisRecepty($result);
    }

    function getUzivatelID($email) {
        $response = $this->db->prepare("SELECT uzivatel_id, hashed FROM uzivatel WHERE email = ?");
        $response->execute(array($email));
        if ($response->rowCount() > 0) {
            $result = $response->fetch();
            return $result;
        } else {
            return null;
        }
    }

    function vlozUzivatele($email, $firstname, $surname, $hashed) {
        $stmt = $this->db->prepare("INSERT INTO uzivatel(email, jmeno, prijmeni, hashed) 
                              VALUES (?, ?, ?, ?)");
        try {
            return $stmt->execute(array($email, $firstname, $surname, $hashed));
        } catch (Exception $e) {
           echo "Chyba při vkládání!<br>";
        }
    }

    function vlozRecept($nazev, $popis, $zeme, $delka, $img_url, $uzivatel_id)
    {
        $stmt = $this->db->prepare("INSERT INTO recept(nazev, popis, zeme_puvodu,
                                  delka_pripravy, hodnoceni, obtiznost, datum_pridani,
                                  img_name, uzivatel_id) 
                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

        $stmt->execute(array($nazev, $popis, $zeme, $delka, 100, 0, null, $img_url, $uzivatel_id));
        return $this->db->lastInsertId();
    }

    function aktualizujRecept($recept_id, $nazev, $popis, $zeme, $delka) {
        $stmt = $this->db->prepare("UPDATE recept
                                              SET nazev=?, popis=?, zeme_puvodu=?, delka_pripravy=?
                                              WHERE recept_id=?");
        $stmt->execute(array($nazev, $popis, $zeme, $delka, $recept_id));
    }

    function smazRecept($receptID) {
        $response = $this->db->prepare("DELETE FROM recept WHERE recept_id=?");
        return $response->execute(array($receptID));
    }

    function najdiReceptPodleNazvu($nazev) {
        $response = $this->db->prepare("SELECT * FROM recept WHERE nazev=? LIMIT 1");
        $response->execute(array($nazev));
        return $response->fetch();
    }

    function vlozIngredience($ingredience, $mnozstvi, $jednotky, $recept_id) {
        $stmt = $this->db->prepare("INSERT INTO ingredience(nazev, jednotky) 
                                  VALUES (?, ?)");
        try {
            $stmt->execute(array($ingredience, $jednotky));
        } catch(Exception $e) {
            //Error při vkládání ingredience, která se již nachází v databázi
        }
        $ingredience_id = $this->db->lastInsertId();


        $stmt = $this->db->prepare("INSERT INTO recept_ma_ingredience(recept_id, ingredience_id, mnozstvi) 
                                  VALUES (?, ?, ?)");
        $stmt->execute(array($recept_id, $ingredience_id, $mnozstvi));

    }

    function smazIngredienceReceptID($recept_id) {
        $stmt = $this->db->prepare("DELETE FROM recept_ma_ingredience WHERE recept_id=?");
        $stmt->execute(array($recept_id));
    }

    function getIngredience($recept_id) {
        $response = $this->db->prepare("SELECT nazev, mnozstvi, jednotky 
                                                  FROM ingredience JOIN (SELECT * FROM recept_ma_ingredience WHERE recept_id=?) 
                                                  vybrane_ingredience USING(ingredience_id)");
        $response->execute(array($recept_id));
        return $response->fetchAll();
    }

    function getReceptyID($id) {
        $response = $this->db->prepare("SELECT * FROM recept WHERE uzivatel_id=?");
        $response->execute(array($id));
        $this->vypisRecepty($response);
    }

    function getPocetReceptu() {
        $total = $this->db->query('SELECT COUNT(*) FROM recept')->fetchColumn();
        return $total;
    }

    function getReceptyFiltr($zeme, $poradi) {
        if ($poradi == "cas") {
            $response = $this->db->prepare("SELECT * FROM recept WHERE zeme_puvodu=? ORDER BY datum_pridani DESC LIMIT 9");
            $response->execute(array($zeme));
            $this->vypisRecepty($response);
        } else if ($poradi == "hodnoceni") {
            $response = $this->db->prepare("SELECT * FROM recept WHERE zeme_puvodu=? ORDER BY hodnoceni DESC LIMIT 9");
            $response->execute(array($zeme));
            $this->vypisRecepty($response);
        } else if ($poradi == "rychlost") {
            $response= $this->db->prepare("SELECT * FROM recept WHERE zeme_puvodu=? ORDER BY delka_pripravy ASC LIMIT 9");
            $response->execute(array($zeme));
            $this->vypisRecepty($response);
        } else {
            echo $poradi;
            exit;
        }
    }

    function getReceptByID($idRecept)
    {
        $idRecept = intval($idRecept);
        $response = $this->db->prepare("SELECT * FROM recept WHERE recept_id=?");
        $response->execute(array($idRecept));
        return $response->fetch(PDO::FETCH_ASSOC);
    }

    function getThumbsByID($idRecept)
    {
        $idRecept = intval($idRecept);
        $response = $this->db->prepare("SELECT thumbs_up, thumbs_down FROM recept WHERE recept_id=? LIMIT 1");
        $response->execute(array($idRecept));
        return $response->fetch();
    }

    function thumb_up($idRecept) {
        $idRecept = intval($idRecept);
        $response = $this->db->prepare("UPDATE recept SET thumbs_up = thumbs_up + 1 WHERE recept_id=?");
        $response->execute(array($idRecept));
    }

    function thumb_down($idRecept) {
        $idRecept = intval($idRecept);
        $response = $this->db->prepare("UPDATE recept SET thumbs_down = thumbs_down + 1 WHERE recept_id=?");
        $response->execute(array($idRecept));
    }

    function pridejDoOblibenych($uzivatel_id, $recept_id) {
        //Existuje záznam?
        $zaznamExistuje = $this->db->prepare("SELECT * FROM uzivatel_ma_oblibene WHERE uzivatel_id=? AND recept_id=?");
        $zaznamExistuje->execute(array($uzivatel_id, $recept_id));
        if ($zaznamExistuje->rowCount() > 0) {
            //smazání záznamu
            $stmt = $this->db->prepare("DELETE FROM uzivatel_ma_oblibene WHERE uzivatel_id=? AND recept_id=?");
            $stmt->execute(array($uzivatel_id, $recept_id));
            return "uspech-oblibene-odebran";
        } else {
            //Vložení záznamu
            $response = $this->db->prepare("INSERT INTO uzivatel_ma_oblibene(uzivatel_id, recept_id) VALUES (?, ?)");
            if($response->execute(array($uzivatel_id, $recept_id))) {
                return "uspech-oblibene-pridan";
            } else {
                return "neuspech-oblibene-nepridan";
            }
        }
    }

    function getOblibeneRecepty() {
        if(isset($_SESSION["user_id"])) {
            $uzivatel_id = $_SESSION["user_id"];
            $response = $this->db->query("SELECT vybranyUzivatel.uzivatel_id, recept_id, nazev, popis, zeme_puvodu, delka_pripravy, hodnoceni, img_name FROM recept 
                                                    JOIN uzivatel_ma_oblibene USING(recept_id) 
                                                    JOIN (SELECT * FROM uzivatel WHERE uzivatel_id=$uzivatel_id) vybranyUzivatel
                                                    ON uzivatel_ma_oblibene.uzivatel_id = vybranyUzivatel.uzivatel_id")->fetchAll();
            $this->vypisRecepty($response);
        } else {
            exit;
        }
    }

    function getOblibeneReceptyID() {
        if (isset($_SESSION["user_id"])) {
            $uzivatel_id = $_SESSION["user_id"];
            $response = $this->db->query("SELECT recept_id FROM uzivatel_ma_oblibene WHERE uzivatel_id=$uzivatel_id ");
            $recepty = $response->fetchAll(PDO::FETCH_COLUMN, 0);
            return $recepty;
        } else {;
            return null;
        }
    }

    function getUzivatelByReceptID($id) {
        $idRecept = intval($id);
        $response = $this->db->prepare("SELECT uzivatel_id FROM recept WHERE recept_id=? LIMIT 1");
        $response->execute(array($idRecept));
        $response = $response->fetch();
        $uzivatelID = $response["uzivatel_id"];
        return $uzivatelID;

    }

    function getNumberUzivateleOblibene($recept_id) {
        $response = $this->db->prepare("SELECT COUNT(uzivatel_id) FROM uzivatel_ma_oblibene WHERE recept_id=?");
        $response->execute(array($recept_id));
        $number =  $response->fetch();
        return $number[0];
    }

    /**
     * @param $response
     */
    private function vypisRecepty($response) {

        /** @var TYPE_NAME $vsechyRecepty */
        $vsechyRecepty = "";
        foreach ($response as $recept) {
            $id = htmlspecialchars($recept["recept_id"]);
            $nazev = htmlspecialchars($recept["nazev"]);
            $znacka = htmlspecialchars($recept["zeme_puvodu"]);
            $delka = htmlspecialchars($recept["delka_pripravy"]);
            $obrazek = htmlspecialchars($recept["img_name"]);
            $hodnoceni = round($recept["hodnoceni"]);
            $oblibeneRecepty = $this->getOblibeneReceptyID();

            $vsechyRecepty .=
                "<div id=\"$id\" class=\"recept $znacka\" style=\"background-image: url('img/$obrazek')\">";

            if(isset($_SESSION["user_id"])) {
                if (in_array($id, $oblibeneRecepty)) {
                    $vsechyRecepty .="<i class=\"material-icons fav clicked\" style='background-color: #c0392b'>bookmark</i>";
                } else {
                    $vsechyRecepty .= "<i class=\"material-icons fav\" style='display: none'>bookmark</i>";
                }
            }

            if($_SERVER['PHP_SELF'] == '/nactiMojeRecepty.php' &&
                $_SESSION["user_id"] == $recept["uzivatel_id"]) {

                $vsechyRecepty .="<i class=\"material-icons delete\" style='display: none'>delete</i>";
                }
                $vsechyRecepty .=
                  "<div class=\"recept_info\">"
                . "<div class=\"popis_receptu\">$nazev</div>"
                . "<div><i class=\"material-icons\">timer</i>: $delka m</div>";


//            for ($i = 0; $i < $hodnoceni; $i++) {

                $vsechyRecepty .= "<div><i class=\"material-icons hodnoceni\">local_dining</i>: $hodnoceni%</div>";;
//            }

                $vsechyRecepty .= "</div> </div> </div>"
                . " ";
        }
        echo $vsechyRecepty;
    }

    function setHodnoceni($thumbs_up, $thumbs_down, $idRecept) {
        $result = 0;
        if ($thumbs_up > 0 && $thumbs_down > 0) {
            $result = ($thumbs_up / ($thumbs_up + $thumbs_down)) * 100;
            if($result > 100) {
                $result = 100;
            }
        } elseif ($thumbs_up = 0) {
            $result = 0;
        } elseif($thumbs_down = 0) {
            $result = 100;
        }
        $response = $this->db->prepare("UPDATE recept SET hodnoceni = ? WHERE recept_id = ?");
        $response->execute(array($result, $idRecept));
        echo round($result);
    }
}

