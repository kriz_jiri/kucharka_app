<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    require 'inc/Database.php';
    $database = new Database();
    $idReceptu = $_SESSION["recept_id"];
    $recept = $database->getReceptByID($idReceptu);
    $ingredience = $database->getIngredience($idReceptu);
?>

<div id="recept">
    <?php if(isset($_SESSION["user_id"]) && $_SESSION["user_id"] == $recept["uzivatel_id"]) { ?>
        <div id="edit-button">
            <i class="material-icons">mode_edit</i>
        </div>
    <?php }?>

    <div id="info">
        <h3><?php echo $recept["nazev"]?></h3>

        <ul id="about">
                <li><i class="material-icons">timer</i> &nbsp; <?php echo $recept["delka_pripravy"]?> minut </li>
                <li><i class="material-icons hodnoceni">local_dining</i> &nbsp;  <?php echo round($recept["hodnoceni"])?>% </li>
                <li><i class="material-icons hodnoceni">bookmark_border</i> &nbsp; <?php echo $database->getNumberUzivateleOblibene($idReceptu)?>x </li>
        </ul>
    </div>
<div id="basic_text">
    <table id="table_ingredience">
        <caption>Ingredience:</caption>
        <?php foreach ($ingredience as $val) {
            $value1 = htmlspecialchars($val[0]);
            $value2 = htmlspecialchars($val[1]);
            $value3 = htmlspecialchars($val[2]);
            echo
                "<tr>"
                ."<td> $value1</td>"
                ."<td>$value2 $value3</td>"
                ."</tr>";
        } ?>
    </table>
</div>
<img src="img/<?php echo htmlspecialchars($recept["img_name"])?>" alt="Image of food">
</div>
<div id="description"><p>Příprava:</p>
    <?php echo htmlspecialchars($recept["popis"])?>
</div>
<div id="icons">
    <i class="material-icons" id="thumb_up">thumb_up</i>
    <i class="material-icons" id="thumb_down">thumb_down</i>
    <?php
        $oblibeneID = $database->getOblibeneReceptyID();
        if(isset($_SESSION["user_id"]) && in_array($idReceptu, $oblibeneID)) {
            echo '<i class="material-icons clicked" id="bookmark" style="background-color: #c0392b">bookmark</i>';
        } else {
            echo '<i class="material-icons" id="bookmark" style="background-color: #e67e22">bookmark</i>';
        }
    ?>
</div>