# KUCHAŘKA #

### O Aplikaci ###

Webová aplikace sloužící k zaznamenání, úpravě a sdílení oblíbených receptů. V aplikaci je možné zobrazovat
recepty ostatních uživatelů, hodnotit je, přidávat je do oblíbených. Recepty je také možné filtrovat a třídit.

### Konfigurace databáze ###

Před spuštěním serveru je nutné vytvořit MySQL databázi.
SQL příkazy určené k vytvoření databáze se nacházejí v souboru database_export.txt