<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 18.03.17
 * Time: 23:04
 */
require 'inc/data.php';
require 'inc/Database.php';
$database = new Database();
$provedenaAktualizace = false;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["nazevReceptu"]) &&
        isset($_POST["popisReceptu"]) &&
        isset($_POST["zeme"]) &&
        isset($_POST["delka"])) {
        $nazevReceptu = trim(filter_input(INPUT_POST, "nazevReceptu", FILTER_SANITIZE_STRING));
        $popisReceptu = trim(filter_input(INPUT_POST, "popisReceptu", FILTER_SANITIZE_STRING));
        $zemePuvodu = trim(filter_input(INPUT_POST, "zeme", FILTER_SANITIZE_STRING));
        $delkaPripravy = filter_input(INPUT_POST, "delka", FILTER_SANITIZE_NUMBER_INT);
        $ingredienceCislo = 0;
        $receptID = -1;


        $validextensions = array("jpeg", "jpg", "png");
        $temporary = explode(".", $_FILES["image"]["name"]);
        $file_extension = end($temporary);

        //Kontrola, zda nahraný soubor je obrázek
        if ((($_FILES["image"]["type"] == "image/png") ||
                ($_FILES["image"]["type"] == "image/jpg") ||
                ($_FILES["image"]["type"] == "image/jpeg")) &&
            in_array($file_extension, $validextensions)) {

            //Uložení obrázku na server
            $uploaddir = 'img/';
            $uploadfile = $uploaddir . basename($_FILES['image']['name']);
            $obrazekNahran = move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);

        } else {
            echo "neni_obrazek";
            exit;
        }

        if ($nazevReceptu == "" || $popisReceptu== "" || $zemePuvodu== "" || $delkaPripravy== null) {
            echo "chyba-vlozeni";

        } else {
            //validace, zda se vkládaný recept již nenachází v databázi
            if(isset($_SESSION["recept_id"])) {
                $vkladanyRecept = $database->getReceptByID($_SESSION["recept_id"]);
                if ($vkladanyRecept > 0 && $_SESSION["user_id"] == $vkladanyRecept["uzivatel_id"]) {
                    $receptID = $_SESSION["recept_id"];
                    $database->aktualizujRecept($receptID, $nazevReceptu, $popisReceptu, $zemePuvodu, $delkaPripravy);
                    $database->smazIngredienceReceptID($receptID);
                    $provedenaAktualizace = true;
                }
            } else {

                //Kontrola, zda se v databázi nenachází recept se stejným názvem
                $recept = $database->najdiReceptPodleNazvu($nazevReceptu);

                if ($recept > 0) {
                    echo "Recept se stejným názvem se již nachází v databázi! Zvolte jiný název";
                    exit;
                } else {

                    //Uložení nového receptu na server
                    $receptID = $database->vlozRecept($nazevReceptu,
                        $popisReceptu,
                        $zemePuvodu,
                        $delkaPripravy,
                        $_FILES['image']['name'],
                        $_SESSION["user_id"]);
                }
            }

            //        Vkládání ingrediencí do databáze
            while(true) {
                if(isset($_POST["ingredience$ingredienceCislo"]) && isset($_POST["mnozstvi$ingredienceCislo"]) && isset($_POST["jednotky$ingredienceCislo"]))     {

                    $ingredience = trim(filter_input(INPUT_POST, "ingredience$ingredienceCislo", FILTER_SANITIZE_STRING));
                    $mnozstvi = trim(filter_input(INPUT_POST, "mnozstvi$ingredienceCislo", FILTER_SANITIZE_NUMBER_INT));
                    $jednotky = trim(filter_input(INPUT_POST, "jednotky$ingredienceCislo", FILTER_SANITIZE_STRING));

                    $database->vlozIngredience($ingredience, $mnozstvi, $jednotky, $receptID);
                } else {
                    break;
                }
                $ingredienceCislo++;
            }

            if ($receptID > -1 && $obrazekNahran) {
                if(isset($_SESSION["recept_id"])) {
                    $_SESSION["recept_id"] = $receptID;
                }
                if ($provedenaAktualizace) {
                    echo "aktualizace";
                } else {
                    echo "uspech";
                }
            }
        }

    }
}

