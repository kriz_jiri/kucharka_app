<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 15.04.17
 * Time: 8:30
 */
session_start();
require 'inc/Database.php';
$database = new Database();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $receptID = $_SESSION["recept_id"];
    $recept = $database->getReceptByID($receptID);
    $ingredience = $database->getIngredience($receptID);
    $ingredience = json_encode($ingredience);
    $recept = implode("?", $recept);
    $odpoved = array($recept, $ingredience);
    echo implode("!!",$odpoved);
}