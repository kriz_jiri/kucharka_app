<?php
session_start();
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 08.04.17
 * Time: 21:54
 */
require 'inc/Database.php';
$database = new Database();

if(($_SERVER["REQUEST_METHOD"] == "GET")){
    if (isset($_SESSION["user"])) {
        $database->getReceptyID($_SESSION["user_id"]);
    } elseif(!isset($_SESSION["user"])) {
        echo "<h1>Není přihlášen žádný uživatel</h1>";
    } else {
        echo "<h1>Zatím nebyly vloženy žádné recepty</h1>";
    }
}