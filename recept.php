<?php
session_start();
if($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET["id"])) {
        $idReceptu = trim(filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT));
        $_SESSION["recept_id"] = $idReceptu;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
        <meta charset="UTF-8">
        <title>Kuchařka</title>
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="favicon/manifest.json">
        <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="favicon/favicon.ico">
        <meta name="msapplication-config" content="favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style-moje-recepty.css">
        <link rel="stylesheet" href="css/style-sign-form.css">
        <link rel="stylesheet" href="css/style-recept-form.css">
        <link rel="stylesheet" href="css/style-recept.css">
        <link rel="stylesheet" href="css/header-footer.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    </head>
    <body>
    <?php   require 'inc/header.php';
            require 'inc/sign-form.php';
            require 'inc/recept-form.php' ?>
    <div class="alert alert-danger" id="chyba"></div>
    <div class="alert alert-success" id="uspech"></div>

    <div id="content">
        <?php require 'nactiRecept.php' ?>
    </div>
    </body>
    <div id="pozadiReceptu" onclick="zavriRecept()"></div>
    <?php require 'inc/footer.php';?>
    </body>
    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/signup.js"></script>
    <script src="js/moje-recepty.js"></script>
</html>

