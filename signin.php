<?php
session_start();
require 'inc/Uzivatele.php';
$uzivatel = new Uzivatele();


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["username"]) && isset($_POST["password"])) {
        $username = trim(filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING));
        $password = trim(filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING));
        $uzivatel->prihlasUzivatele($username, $password);
    } else {
        //header("Location: /index.php");
    }
} else {
    if (isset($_GET["logout"])) {
        $uzivatel->odhlasUzivatele();
    }
}