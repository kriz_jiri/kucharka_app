-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost:8889
-- Vytvořeno: Stř 01. lis 2017, 13:13
-- Verze serveru: 5.6.35
-- Verze PHP: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `kucharka`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `ingredience`
--

CREATE TABLE `ingredience` (
  `ingredience_id` int(6) NOT NULL,
  `nazev` varchar(40) NOT NULL,
  `jednotky` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `recept`
--

CREATE TABLE `recept` (
  `recept_id` int(11) NOT NULL,
  `nazev` varchar(40) NOT NULL,
  `popis` text NOT NULL,
  `zeme_puvodu` varchar(30) NOT NULL,
  `delka_pripravy` int(11) NOT NULL,
  `hodnoceni` float NOT NULL,
  `obtiznost` int(1) NOT NULL,
  `datum_pridani` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `img_name` varchar(100) NOT NULL,
  `uzivatel_id` int(6) NOT NULL,
  `thumbs_up` int(11) NOT NULL,
  `thumbs_down` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `recept_ma_ingredience`
--

CREATE TABLE `recept_ma_ingredience` (
  `recept_id` int(11) NOT NULL,
  `ingredience_id` int(11) NOT NULL,
  `mnozstvi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `uzivatel`
--

CREATE TABLE `uzivatel` (
  `uzivatel_id` int(6) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jmeno` varchar(30) NOT NULL,
  `prijmeni` varchar(30) NOT NULL,
  `hashed` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `uzivatel_ma_oblibene`
--

CREATE TABLE `uzivatel_ma_oblibene` (
  `uzivatel_id` int(11) NOT NULL,
  `recept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `ingredience`
--
ALTER TABLE `ingredience`
  ADD PRIMARY KEY (`ingredience_id`);

--
-- Klíče pro tabulku `recept`
--
ALTER TABLE `recept`
  ADD PRIMARY KEY (`recept_id`),
  ADD UNIQUE KEY `nazev` (`nazev`);

--
-- Klíče pro tabulku `recept_ma_ingredience`
--
ALTER TABLE `recept_ma_ingredience`
  ADD KEY `recept_id` (`recept_id`),
  ADD KEY `ingredience_id` (`ingredience_id`);

--
-- Klíče pro tabulku `uzivatel`
--
ALTER TABLE `uzivatel`
  ADD PRIMARY KEY (`uzivatel_id`);

--
-- Klíče pro tabulku `uzivatel_ma_oblibene`
--
ALTER TABLE `uzivatel_ma_oblibene`
  ADD KEY `uzivatel_id` (`uzivatel_id`),
  ADD KEY `recept_id` (`recept_id`);

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `recept_ma_ingredience`
--
ALTER TABLE `recept_ma_ingredience`
  ADD CONSTRAINT `recept_ma_ingredience_ibfk_1` FOREIGN KEY (`recept_id`) REFERENCES `recept` (`recept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recept_ma_ingredience_ibfk_2` FOREIGN KEY (`ingredience_id`) REFERENCES `ingredience` (`ingredience_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `uzivatel_ma_oblibene`
--
ALTER TABLE `uzivatel_ma_oblibene`
  ADD CONSTRAINT `uzivatel_ma_oblibene_ibfk_1` FOREIGN KEY (`recept_id`) REFERENCES `recept` (`recept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `uzivatel_ma_oblibene_ibfk_2` FOREIGN KEY (`uzivatel_id`) REFERENCES `uzivatel` (`uzivatel_id`) ON DELETE CASCADE ON UPDATE CASCADE;
