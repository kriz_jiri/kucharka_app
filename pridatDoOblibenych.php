<?php
/**
 * Created by PhpStorm.
 * User: jirikriz
 * Date: 14.05.17
 * Time: 11:46
 */
session_start();
require 'inc/Database.php';
$database = new Database();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $receptID = null;

    if (isset($_POST["recept_id"])) {
        $receptID = $_POST["recept_id"]; //v případě, kdy je recept vybrán z menu
    } else if (isset($_SESSION["recept_id"])) {
        $receptID = $_SESSION["recept_id"]; //v případě že je recept přímo z receptu
    } else {
        echo "chyba-neni-prihlasen";
        exit;
    }

    if (isset($_SESSION["user_id"])) {
        $uzivatelID = $_SESSION["user_id"];
        $odpoved = $database->pridejDoOblibenych($uzivatelID, $receptID);
        echo $odpoved;
    } else {
        echo "chyba-neni-prihlasen";
    }
}