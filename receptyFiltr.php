<?php
require "inc/Database.php";
$database = new Database();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET["zeme"]) && isset($_GET["order"])) {
        $zeme = filter_input(INPUT_GET, "zeme", FILTER_SANITIZE_STRING);
        $order = filter_input(INPUT_GET, "order", FILTER_SANITIZE_STRING);
        if ($zeme != "" && $order != "") {
            $database->getReceptyFiltr($zeme, $order);
            exit;
        }
    }
    $database->getRecepty(0);
}